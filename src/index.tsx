import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './Components/App';
import { AppContainer } from 'react-hot-loader';
import './main.scss';
import ToolsRegistry from "./Domain/Tools/ToolsRegistry";
import Brush from "./Domain/Tools/Creators/Brush";
import Line from "./Domain/Tools/Creators/Line";
import Square from "./Domain/Tools/Creators/Square";
import Circle from "./Domain/Tools/Creators/Circle";
import Text from "./Domain/Tools/Creators/Text";
import Drag from "./Domain/Tools/Modifiers/Drag";
import CanvasSize from "./Components/Canvas/CanvasSize";
import FillColor from "./Domain/Tools/Modifiers/FillColor";
import { library } from '@fortawesome/fontawesome-svg-core'
import {
    faEye,
    faEyeSlash,
    faLink,
    faSlash,
    faSquare,
    faCircle,
    faFont,
    faTrashAlt,
    faPaintBrush,
    faArrowsAlt,
    faFillDrip, faCaretUp, faCaretDown, faEdit
} from '@fortawesome/free-solid-svg-icons'
import EditContent from "./Domain/Tools/Modifiers/EditContent";

library.add(faTrashAlt);
library.add(faEye);
library.add(faEyeSlash);
library.add(faLink);
library.add(faSlash);
library.add(faSquare);
library.add(faCircle);
library.add(faFont);
library.add(faPaintBrush);
library.add(faPaintBrush);
library.add(faArrowsAlt);
library.add(faFillDrip);
library.add(faCaretUp);
library.add(faCaretDown);
library.add(faEdit);

const toolsRegistry = new ToolsRegistry();
toolsRegistry.registerCreator(new Brush());
toolsRegistry.registerCreator(new Line());
toolsRegistry.registerCreator(new Square());
toolsRegistry.registerCreator(new Circle());
toolsRegistry.registerCreator(new Text());
toolsRegistry.registerModifier(new Drag());
toolsRegistry.registerModifier(new FillColor());
toolsRegistry.registerModifier(new EditContent());

const Container = () => {
    return (
        <AppContainer>
            <App toolsRegistry={toolsRegistry} canvasSize={new CanvasSize(800, 800)}/>
        </AppContainer>
    );
};

if (module.hot) {
    module.hot.accept();
}

ReactDOM.render(
    <Container />,
    document.getElementById('root')  as HTMLElement
);
