import ModifierInterface from "../Tools/Modifiers/ModifierInterface";
import Drag from "../Tools/Modifiers/Drag";
import Size from "./Common/Size";
import * as React from "react";
import Coords from "./Common/Coords";
import {Brush} from "../../Components/Layers/Brush";
import AbstractLayer from "./Abstractions/AbstractLayer";
import Color from "../Tools/Settings/Color/Color";
import StrokeSize from "../Tools/Settings/Size/Size";

export default class BrushLayer extends AbstractLayer {
    private inDraggingMode = false;
// @ts-ignore
    private className = this.constructor.name;

    public constructor(
        private position: Coords, private pathCoords: Coords[], private color: Color, private size: StrokeSize) {
        super();
    }

    protected getSupportedModifiers(): any[] {
        return [
            Drag
        ];
    }

    public activateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof Drag) {
            this.inDraggingMode = true;
        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
    }

    public deactivateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof Drag) {
            this.inDraggingMode = false;
        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
    }

    public getSize(): Size {
        return new Size(0,0);
    }

    public isInDragMode = (): boolean => {
        return this.inDraggingMode;
    };

    public updatePosition = (position: Coords, pathCoords: Coords[]) => {
        this.position = position;
        this.pathCoords = pathCoords;
    };

    public asComponent(attributes: {}): React.SVGProps<any> {
        return <Brush
            key={this.identifier}
            position={this.position}
            pathCoords={this.pathCoords}
            isInDragMode={this.isInDragMode}
            updatePosition={this.updatePosition}
            color={this.color}
            size={this.size}
            attributes={attributes}
        /> as React.SVGProps<any>;
    }

    public getName(): string {
        return "Brush";
    }

    /**
     * @throws Error - if object has not sufficient data
     * @param object
     */
    public static createFromObject(object: any): BrushLayer {
        if (
            object.inDraggingMode  !== undefined &&
            object.position !== undefined &&
            object.pathCoords !== undefined &&
            object.color !== undefined &&
            object.size !== undefined
        ) {
            const coords: Coords[] = [];
            object.pathCoords.forEach((pathCoord: object) => {
               coords.push(Coords.createFromObject(pathCoord));
            });
            return Object.assign(
                new BrushLayer(
                    Coords.createFromObject(object.position),
                    coords,
                    Color.createFromObject(object.color),
                    StrokeSize.createFromObject(object.strokeSize)
                ),
                object,
                {
                    position: Coords.createFromObject(object.position),
                    pathCoords: coords,
                    strokeSize: StrokeSize.createFromObject(object.strokeSize),
                    color: Color.createFromObject(object.color)
                }
            );
        } else {
            throw new Error(`Insufficient data for BrushLayer. Provided: ${JSON.stringify(object)}`);
        }
    }
}
