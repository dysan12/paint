import ModifierInterface from "../Tools/Modifiers/ModifierInterface";
import CanvasSize from "../../Components/Canvas/CanvasSize";
import Size from "./Common/Size";
import * as React from "react";
import AbstractLayer from "./Abstractions/AbstractLayer";
import Color from "../Tools/Settings/Color/Color";
import {instanceOfColorableInterface} from "../Tools/Settings/Color/ColorableInterface";
import FillColor from "../Tools/Modifiers/FillColor";
import {Square} from "../../Components/Layers/Square";
import Coords from "./Common/Coords";

export class BackgroundLayer extends AbstractLayer {
    private readonly size: Size;
    private inFillColorMode = false;
    private activeModifier: ModifierInterface | undefined;
    private color = new Color(255, 255, 255);
    private position = new Coords(0, 0);
    // @ts-ignore
    private className = this.constructor.name;

    public constructor(canvasSize: CanvasSize) {
        super();
        this.size = new Size(canvasSize.getWidth(), canvasSize.getHeight());
    }

    protected getSupportedModifiers(): any[] {
        return [
            FillColor
        ];
    }

    public getSize(): Size {
        return this.size;
    }

    public asComponent(attributes: {} = {}): React.SVGProps<any> {
        return <Square
            key={this.identifier}
            position={this.position}
            width={this.size.width}
            height={this.size.height}
            isInDragMode={() => false}
            color={this.color}
            isInFillColorMode={this.isInFillColorMode}
            getColor={this.getColor}
            updatePosition={(position: Coords) => {
            }}
            attributes={attributes}
        /> as React.SVGProps<any>
    }

    public isInFillColorMode = (): boolean => {
        return this.inFillColorMode;
    };

    public getColor = (): Color => {
        if (instanceOfColorableInterface(this.activeModifier)) {
            this.color = this.activeModifier.getColor();
        }

        return this.color;
    };

    public activateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof FillColor) {
            this.inFillColorMode = true;
            this.activeModifier = modifier;

        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
    }

    public deactivateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof FillColor) {
            this.inFillColorMode = false;
        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
        this.activeModifier = undefined;
    }

    public getName(): string {
        return "Background";
    }

    /**
     * @throws Error - if object has not sufficient data
     * @param object
     */
    public static createFromObject(object: any): BackgroundLayer {
        if (
            object.size !== undefined &&
            object.inFillColorMode !== undefined &&
            object.color !== undefined &&
            object.position !== undefined
        ) {
            return Object.assign(
                new BackgroundLayer(CanvasSize.createFromObject(object.size)),
                object,
                {
                    activeModifier: undefined,
                    color: Color.createFromObject(object.color),
                    position: Coords.createFromObject(object.position),
                    size: CanvasSize.createFromObject(object.size)
                }
            );
        } else {
            throw new Error(`Insufficient data for BackgroundLayer. Provided: ${JSON.stringify(object)}`);
        }
    }
}
