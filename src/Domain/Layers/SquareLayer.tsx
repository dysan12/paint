import ModifierInterface from "../Tools/Modifiers/ModifierInterface";
import Drag from "../Tools/Modifiers/Drag";
import Size from "./Common/Size";
import * as React from "react";
import {Square} from "../../Components/Layers/Square";
import FillColor from "../Tools/Modifiers/FillColor";
import AbstractLayer from "./Abstractions/AbstractLayer";
import Coords from "./Common/Coords";
import Color from "../Tools/Settings/Color/Color";
import {instanceOfColorableInterface} from "../Tools/Settings/Color/ColorableInterface";

export default class SquareLayer extends AbstractLayer{
    private inDraggingMode = false;
    private inFillColorMode = false;
    private activeModifier: ModifierInterface|undefined;
    // @ts-ignore
    private className = this.constructor.name;

    public constructor(private position: Coords, private size: Size, private color: Color) {
        super();
    }

    protected getSupportedModifiers(): any[] {
        return [
            Drag, FillColor
        ];
    }

    public isInDragMode = (): boolean => {
        return this.inDraggingMode;
    };

    public updatePosition = (position: Coords) => {
        this.position = position;
    };

    public getSize(): Size {
        return this.size;
    }

    public asComponent(attributes: {} = {}): React.SVGProps<any> {
        return <Square
            key={this.identifier}
            position={this.position}
            width={this.size.width}
            height={this.size.height}
            isInDragMode={this.isInDragMode}
            color={this.color}
            isInFillColorMode={this.isInFillColorMode}
            getColor={this.getColor}
            updatePosition={this.updatePosition}
            attributes={attributes}
        /> as React.SVGProps<any>
    }

    public isInFillColorMode = (): boolean => {
        return this.inFillColorMode;
    };

    public getColor = (): Color => {
        if (instanceOfColorableInterface(this.activeModifier)) {
            this.color = this.activeModifier.getColor();
        }

        return this.color;
    };

    public activateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof Drag) {
            this.inDraggingMode = true;
        } else if (modifier instanceof FillColor){
            this.inFillColorMode = true;
            this.activeModifier = modifier;

        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
    }

    public deactivateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof Drag) {
            this.inDraggingMode = false;
        } else if (modifier instanceof FillColor){
            this.inFillColorMode = false;
        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
        this.activeModifier = undefined;
    }

    public getName(): string {
        return "Square";
    }


    /**
     * @throws Error - if object has not sufficient data
     * @param object
     */
    public static createFromObject(object: any): SquareLayer {
        if (object.position  !== undefined && object.size  !== undefined && object.color  !== undefined && object.inDraggingMode  !== undefined && object.inFillColorMode !== undefined ) {
            return Object.assign(
                new SquareLayer(
                    Coords.createFromObject(object.position),
                    Size.createFromObject(object.size),
                    Color.createFromObject(object.color)
                ),
                object,
                {
                    activeModifier: undefined,
                    position: Coords.createFromObject(object.position),
                    size: Size.createFromObject(object.size),
                    color: Color.createFromObject(object.color)
                }
            );
        } else {
            throw new Error(`Insufficient data for CircleLayer. Provided: ${JSON.stringify(object)}`);
        }
    }
}
