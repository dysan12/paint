import ModifierInterface from "../Tools/Modifiers/ModifierInterface";
import Drag from "../Tools/Modifiers/Drag";
import Size from "./Common/Size";
import * as React from "react";
import {Text} from "../../Components/Layers/Text";
import FillColor from "../Tools/Modifiers/FillColor";
import AbstractLayer from "./Abstractions/AbstractLayer";
import Coords from "./Common/Coords";
import Color from "../Tools/Settings/Color/Color";
import StrokeSize from "../Tools/Settings/Size/Size";
import {instanceOfColorableInterface} from "../Tools/Settings/Color/ColorableInterface";
import EditContent from "../Tools/Modifiers/EditContent";

export default class TextLayer extends AbstractLayer{
    private inDraggingMode = false;
    private inFillColorMode = false;
    private inEditContentMode = false;
    private activeModifier: ModifierInterface|undefined;
    private size = new Size(1, 1);
    private content = ["Text"];
    // @ts-ignore
    private className = this.constructor.name;

    public constructor(private position: Coords, private color: Color, private strokeSize: StrokeSize) {
        super();
    }

    protected getSupportedModifiers(): any[] {
        return [
            Drag, FillColor, EditContent
        ];
    }

    public isInDragMode = (): boolean => {
        return this.inDraggingMode;
    };

    public isInEditContentMode = (): boolean => {
        return this.inEditContentMode;
    };

    public updateContent = (content: string[]): void => {
        this.content = content;
    };

    public updatePosition = (position: Coords) => {
        this.position = position;
    };

    public getSize(): Size {
        return this.size;
    }

    public getStrokeSize(): StrokeSize {
        return this.strokeSize
    }

    public asComponent(attributes: {} = {}): React.SVGProps<any> {
        return <Text
            key={this.identifier}
            position={this.position}
            isInDragMode={this.isInDragMode}
            color={this.color}
            size={this.strokeSize}
            isInFillColorMode={this.isInFillColorMode}
            content={this.content}
            isInEditContentMode={this.isInEditContentMode}
            updateContent={this.updateContent}
            getColor={this.getColor}
            updatePosition={this.updatePosition}
            attributes={attributes}
        /> as React.SVGProps<any>
    }

    public isInFillColorMode = (): boolean => {
        return this.inFillColorMode;
    };

    public getColor = (): Color => {
        if (instanceOfColorableInterface(this.activeModifier)) {
            this.color = this.activeModifier.getColor();
        }

        return this.color;
    };

    public activateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof Drag) {
            this.inDraggingMode = true;
        } else if (modifier instanceof FillColor){
            this.inFillColorMode = true;
            this.activeModifier = modifier;
        } else if (modifier instanceof EditContent) {
            this.inEditContentMode = true;
        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
    }

    public deactivateModifier(modifier: ModifierInterface): void {
        if (modifier instanceof Drag) {
            this.inDraggingMode = false;
        } else if (modifier instanceof FillColor){
            this.inFillColorMode = false;
        } else if (modifier instanceof EditContent) {
            this.inEditContentMode = false;
        } else {
            throw new Error("Provided modifier is not supported by layer");
        }
        this.activeModifier = undefined;
    }

    public getName(): string {
        return "Text";
    }

    /**
     * @throws Error - if object has not sufficient data
     * @param object
     */
    public static createFromObject(object: any): TextLayer {
        if (
            object.position !== undefined &&
            object.color !== undefined &&
            object.strokeSize !== undefined &&
            object.inDraggingMode !== undefined &&
            object.inFillColorMode !== undefined &&
            object.inEditContentMode !== undefined &&
            object.size !== undefined &&
            object.content !== undefined
        ) {
            return Object.assign(
                new TextLayer(
                    Coords.createFromObject(object.position),
                    Color.createFromObject(object.color),
                    StrokeSize.createFromObject(object.strokeSize)
                ),
                object,
                {
                    activeModifier: undefined,
                    position: Coords.createFromObject(object.position),
                    size: Size.createFromObject(object.size),
                    color: Color.createFromObject(object.color),
                    strokeSize: StrokeSize.createFromObject(object.strokeSize)
                }
            );
        } else {
            throw new Error(`Insufficient data for BrushLayer. Provided: ${JSON.stringify(object)}`);
        }
    }
}
