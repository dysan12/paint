export default class Size {
    public constructor (public width: number, public height: number) {};

    public static createFromObject(object: any): Size {
        if (object.width !== undefined && object.height!== undefined) {
            return new Size(parseInt(object.width, 10), parseInt(object.height, 10));
        } else {
            throw new Error(`Insufficient data for Size. Provided: ${JSON.stringify(object)}`);
        }
    }
}
