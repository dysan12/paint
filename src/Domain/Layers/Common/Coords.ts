export default class Coords {
    public constructor(private readonly _x: number, private readonly _y: number) {}

    get x(): number {
        return this._x;
    }

    get y(): number {
        return this._y;
    }

    public static createFromObject(object: any): Coords {
        if (object._x !== undefined && object._y !== undefined) {
            return new Coords(parseInt(object._x, 10), parseInt(object._y, 10));
        } else {
            throw new Error(`Insufficient data for Coords. Provided: ${JSON.stringify(object)}`);
        }
    }
}
