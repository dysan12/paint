import { BackgroundLayer } from "./BackgroundLayer";
import BrushLayer from "./BrushLayer";
import CircleLayer from "./CircleLayer";
import LineLayer from "./LineLayer";
import SquareLayer from "./SquareLayer";
import TextLayer from "./TextLayer";
import LayerInterface from "./Abstractions/LayerInterface";

export default class Deserializer {
    private static serializationMap = {
        BackgroundLayer,
        BrushLayer,
        CircleLayer,
        LineLayer,
        SquareLayer,
        TextLayer
    };

    public static deserializeArray(object: object[]): LayerInterface[] {
        const deserialized: LayerInterface[] = [];
        object.forEach((layer: any) => {
            deserialized.push(this.serializationMap[layer.className].createFromObject(layer));
        });

        return deserialized;
    }

    public static deserialize(json: string): LayerInterface {
        const parsed = JSON.parse(json);

        return this.serializationMap[parsed.className].createFromObject(parsed);
    }
}
