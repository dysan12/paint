export function instanceOfImmutableInterface(object: any): object is ImmutableInterface {
    return object.isImmutable !== undefined && object.changeImmutabilityState !== undefined;
}

export interface ImmutableInterface {
    isImmutable(): boolean;
    changeImmutabilityState(state: boolean): void;
}