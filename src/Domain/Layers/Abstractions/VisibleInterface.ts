export function instanceOfVisibleInterface(object: any): object is VisibleInterface {
    return object.setVisibility !== undefined && object.isVisible !== undefined;
}

export interface VisibleInterface {
    isVisible(): boolean;
    setVisibility(state: boolean): void;
}