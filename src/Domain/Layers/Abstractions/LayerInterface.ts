import ModifierInterface from "../../Tools/Modifiers/ModifierInterface";
import Size from "../Common/Size";
import * as React from "react";

export default interface LayerInterface {
    getIdentifier(): string;
    supportsModifier(modifier: ModifierInterface): boolean;
    activateModifier(modifier: ModifierInterface): void;
    deactivateModifier(modifier: ModifierInterface): void;
    getSize(): Size;
    asComponent(attributes: {}): React.SVGProps<any>;
    getName(): string;
}