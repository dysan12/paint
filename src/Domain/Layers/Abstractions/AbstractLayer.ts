import LayerInterface from "./LayerInterface";
import ModifierInterface from "../../Tools/Modifiers/ModifierInterface";
import Size from "../Common/Size";
import * as React from "react";
import * as uuidv1 from "uuid/v1";
import {VisibleInterface} from "./VisibleInterface";
import {ImmutableInterface} from "./ImmutableInterface";

export default abstract class AbstractLayer implements LayerInterface, VisibleInterface, ImmutableInterface{
    protected identifier: string;
    protected visibility: boolean = true;
    protected immutable: boolean = false;

    protected constructor() {
        this.identifier = uuidv1();
    }

    public abstract activateModifier(modifier: ModifierInterface): void;
    public abstract asComponent(attributes: {}): React.SVGProps<any>;
    public abstract deactivateModifier(modifier: ModifierInterface): void;
    public abstract getSize(): Size;
    public abstract getName(): string;
    protected abstract getSupportedModifiers(): any[];

    public getIdentifier(): string {
        return this.identifier;
    }

    public supportsModifier(modifier: ModifierInterface): boolean {
        let supports = false;
        for (const supportedModifier of this.getSupportedModifiers()) {
            if (modifier instanceof supportedModifier) {
                supports = true;
                break;
            }
        }
        return supports;
    }

    public isVisible(): boolean {
        return this.visibility;
    }

    public isImmutable(): boolean {
        return this.immutable;
    }

    public changeImmutabilityState(state: boolean): void {
        this.immutable = state;
    }

    public setVisibility(state: boolean): void {
        this.visibility = state;
    }
}