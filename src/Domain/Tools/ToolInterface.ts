export default abstract class ToolInterface {
    public abstract getName(): string;
    public abstract getIconName(): string;
}