import ModifierInterface from "./Modifiers/ModifierInterface";
import CreatorInterface from "./Creators/CreatorInterface";
import ToolsRegistryInterface from "./ToolsRegistryInterface";

export default class ToolsRegistry implements ToolsRegistryInterface {
    private modifiers: ModifierInterface[] = [];
    private creators: CreatorInterface[] = [];

    public registerCreator(creator: CreatorInterface): void {
        this.creators.push(creator);
    }

    public getCreators(): CreatorInterface[] {
        return this.creators;
    }

    public registerModifier(modifier: ModifierInterface): void {
        this.modifiers.push(modifier);
    }

    public getModifiers(): ModifierInterface[] {
        return this.modifiers;
    }
}