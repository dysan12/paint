import ModifierInterface from "./Modifiers/ModifierInterface";
import CreatorInterface from "./Creators/CreatorInterface";

export default interface ToolsRegistryInterface {
    getModifiers(): ModifierInterface[];
    getCreators(): CreatorInterface[];
}