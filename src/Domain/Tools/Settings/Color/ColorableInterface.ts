import Color from "./Color";

export function instanceOfColorableInterface(object: any): object is ColorableInterface {
    return object.setColor !== undefined && object.getColor !== undefined;
}

export interface ColorableInterface {
    setColor(color: Color): void;
    getColor(): Color;
}