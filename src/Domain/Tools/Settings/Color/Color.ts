export default class Color {
    public constructor(private readonly _red: number, private readonly _green: number, private readonly _blue: number, private readonly _alpha: number = 1) {}

    get red(): number {
        return this._red;
    }

    get green(): number {
        return this._green;
    }

    get blue(): number {
        return this._blue;
    }

    get alpha(): number {
        return this._alpha;
    }

    public toRgba(): string {
        return `rgba(${this.red}, ${this.green}, ${this.blue}, ${this.alpha})`;
    }

    public static createFromObject(object: any): Color {
        if (object._red  !== undefined && object._green  !== undefined && object._blue  !== undefined && object._alpha !== undefined ) {
            return new Color(parseInt(object._red, 10), parseInt(object._green, 10), parseInt(object._blue, 10), parseFloat(object._alpha))
        } else {
            throw new Error(`Insufficient data for Color. Provided: ${JSON.stringify(object)}`);
        }
    }
}
