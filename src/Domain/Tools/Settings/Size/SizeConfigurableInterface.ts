import Size from "./Size";

export function instanceOfSizeConfigurableInterface(object: any): object is SizeConfigurableInterface {
    return object.setSize !== undefined && object.getSize !== undefined;
}

export interface SizeConfigurableInterface {
    setSize(size: Size): void;
    getSize(): Size;
}