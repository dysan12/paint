export default class Size {
    public constructor(private readonly _size: number, private readonly _unit: string = 'px'){}

    get size(): number {
        return this._size;
    }

    get unit(): string {
        return this._unit;
    }

    public static createFromObject(object: any): Size {
        if (object._size  !== undefined && object._unit  !== undefined) {
            return new Size(parseFloat(object._size), object._unit)
        } else {
            throw new Error(`Insufficient data for Settings/Size. Provided: ${JSON.stringify(object)}`);
        }
    }
}
