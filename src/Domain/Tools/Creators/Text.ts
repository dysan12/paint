import CreatorInterface from "./CreatorInterface";
import LayerInterface from "../../Layers/Abstractions/LayerInterface";
import Coords from "../../Layers/Common/Coords";
import TextLayer from "../../Layers/TextLayer";
import {ColorableInterface} from "../Settings/Color/ColorableInterface";
import Color from "../Settings/Color/Color";
import {SizeConfigurableInterface} from "../Settings/Size/SizeConfigurableInterface";
import StrokeSize from "../Settings/Size/Size";

export default class Text extends CreatorInterface implements ColorableInterface, SizeConfigurableInterface{
    private cursorPositions: Coords[] = [];
    private svgParentElement: SVGSVGElement | undefined;
    private color = new Color(0,0,0);
    private size = new StrokeSize(1);

    private resolveClientCoords(clientX: number, clientY: number): Coords {
        let coords;
        if (this.svgParentElement) {
            const svgBoundaries = this.svgParentElement.getBoundingClientRect();
            coords = new Coords(clientX - svgBoundaries.left, clientY - svgBoundaries.top);
        } else {
            coords = new Coords(clientX, clientY);
        }

        return coords;
    }

    public initialize(event: PointerEvent, svgElement: SVGSVGElement): void {
        this.svgParentElement = svgElement;
        this.cursorPositions.push(this.resolveClientCoords(event.clientX, event.clientY));
    }

    public create(): LayerInterface | undefined {
        let newLayer;
        if (this.svgParentElement) {
            if (this.cursorPositions.length) {
                const startPosition = this.cursorPositions[0];
                newLayer = new TextLayer(startPosition, this.color, this.size);
             }
            this.clear();
        }
        return newLayer;
    }

    public getName(): string {
        return 'Text';
    }

    private clear(): void {
        this.cursorPositions = [];
        this.svgParentElement = undefined;
    }

    public getIconName(): string {
        return "font";
    }

    public setColor(color: Color): void {
        this.color = color;
    }

    public getColor(): Color {
        return this.color;
    }

    public getSize(): StrokeSize {
        return this.size;
    }

    public setSize(size: StrokeSize): void {
        this.size = size;
    }
}
