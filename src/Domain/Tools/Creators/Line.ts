import CreatorInterface from "./CreatorInterface";
import LayerInterface from "../../Layers/Abstractions/LayerInterface";
import Coords from "../../Layers/Common/Coords";
import LineLayer from "../../Layers/LineLayer";
import {ColorableInterface} from "../Settings/Color/ColorableInterface";
import Color from "../Settings/Color/Color";
import {SizeConfigurableInterface} from "../Settings/Size/SizeConfigurableInterface";
import Size from "../Settings/Size/Size";
import { PREVIEW_CANVAS_ID } from "../../../config";

export default class Line extends CreatorInterface implements ColorableInterface, SizeConfigurableInterface{
    private cursorPositions: Coords[] = [];
    private svgParentElement: SVGSVGElement | undefined;
    private color = new Color(0,0,0);
    private size = new Size(1);
    private previewCanvas: undefined | HTMLCanvasElement;

    public onMouseMove = (event: PointerEvent) => {
        this.cursorPositions.push(this.resolveClientCoords(event.clientX, event.clientY));
        this.drawCanvasPreview();
    };

    private drawCanvasPreview() {
        this.clearCanvasPreview();
        if (this.previewCanvas) {
            const context = this.previewCanvas.getContext('2d');
            if (context) {
                const startCoords = this.cursorPositions[0];
                const endCoords = this.cursorPositions[this.cursorPositions.length - 1];

                context.strokeStyle = this.color.toRgba();
                context.lineWidth = this.size.size;
                context.moveTo(startCoords.x, startCoords.y);
                context.lineTo(endCoords.x, endCoords.y);
                context.stroke();
            }
        }
    }

    private clearCanvasPreview() {
        if (this.previewCanvas) {
            const context = this.previewCanvas.getContext('2d');
            if (context) {
                context.beginPath();
                context.clearRect(0, 0, context.canvas.width, context.canvas.height);
            }
        }
    }

    private resolveClientCoords(clientX: number, clientY: number): Coords {
        let coords;
        if (this.svgParentElement) {
            const svgBoundaries = this.svgParentElement.getBoundingClientRect();
            coords = new Coords(clientX - svgBoundaries.left, clientY - svgBoundaries.top);
        } else {
            coords = new Coords(clientX, clientY);
        }

        return coords;
    }

    public initialize(event: PointerEvent, svgElement: SVGSVGElement): void {
        this.svgParentElement = svgElement;
        this.svgParentElement.addEventListener('mousemove', this.onMouseMove);
        this.previewCanvas = document.getElementById(PREVIEW_CANVAS_ID) as HTMLCanvasElement
    }

    public create(): LayerInterface | undefined {
        let newLayer;
        if (this.svgParentElement) {
            this.svgParentElement.removeEventListener('mousemove', this.onMouseMove);
            if (this.cursorPositions.length) {
                newLayer = new LineLayer(this.cursorPositions[0], this.cursorPositions[this.cursorPositions.length-1], this.color, this.size);
            }
            this.clear();
        }
        return newLayer;
    }

    public getName(): string {
        return 'Line';
    }

    private clear(): void {
        this.cursorPositions = [];
        this.svgParentElement = undefined;
        this.clearCanvasPreview();
    }

    public getIconName(): string {
        return "slash";
    }

    public setColor(color: Color): void {
        this.color = color;
    }

    public getColor(): Color {
        return this.color;
    }

    public getSize(): Size {
        return this.size;
    }

    public setSize(size: Size): void {
        this.size = size;
    }
}
