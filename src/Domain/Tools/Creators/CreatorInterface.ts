import LayerInterface from "../../Layers/Abstractions/LayerInterface";
import ToolInterface from "../ToolInterface";

export default abstract class CreatorInterface extends ToolInterface{
    public abstract initialize(event: PointerEvent, svgElement: SVGSVGElement): void;
    public abstract create(): LayerInterface | undefined;
}