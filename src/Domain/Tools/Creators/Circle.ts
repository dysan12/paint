import CreatorInterface from "./CreatorInterface";
import LayerInterface from "../../Layers/Abstractions/LayerInterface";
import Coords from "../../Layers/Common/Coords";
import CircleLayer from "../../Layers/CircleLayer";
import {ColorableInterface} from "../Settings/Color/ColorableInterface";
import Color from "../Settings/Color/Color";
import Size from "../../Layers/Common/Size";
import { PREVIEW_CANVAS_ID } from "../../../config";

export default class Circle extends CreatorInterface implements ColorableInterface{
    private cursorPositions: Coords[] = [];
    private svgParentElement: SVGSVGElement | undefined;
    private color = new Color(0,0,0);
    private previewCanvas: undefined | HTMLCanvasElement;

    public onMouseMove = (event: PointerEvent) => {
        this.cursorPositions.push(this.resolveClientCoords(event.clientX, event.clientY));
        this.drawCanvasPreview();
    };

    private drawCanvasPreview() {
            this.clearCanvasPreview();
            if (this.previewCanvas) {
                const context = this.previewCanvas.getContext('2d');
                if (context) {
                    let startPosition = this.cursorPositions[0];
                    let endPosition = new Size(this.cursorPositions[this.cursorPositions.length-1].x,
                                                this.cursorPositions[this.cursorPositions.length-1].y);
                    let tmpStartX = startPosition.x;
                    let tmpStartY = startPosition.y
                    let tmpEndX = endPosition.width;
                    let tmpEndY = endPosition.height;
                    if (startPosition.x > endPosition.width){
                        const tmpX = tmpStartX;
                        tmpStartX = tmpEndX;
                        tmpEndX = tmpX - tmpEndX;
                        tmpStartX += tmpEndX/2;
                        tmpEndX /= 2;
                    }
                    else {
                        tmpEndX -= tmpStartX;
                        tmpEndX /= 2;
                        tmpStartX += tmpEndX;
                    }
                    if (startPosition.y > endPosition.height){
                        const tmpY = tmpStartY;
                        tmpStartY = tmpEndY;
                        tmpEndY = tmpY - tmpEndY;
                        tmpStartY += tmpEndY/2;
                        tmpEndY /= 2;
                    }
                    else {
                        tmpEndY -= tmpStartY;
                        tmpEndY /= 2;
                        tmpStartY += tmpEndY;
                    }

                    startPosition = new Coords(tmpStartX, tmpStartY);
                    endPosition = new Size(tmpEndX, tmpEndY);

                    context.fillStyle = this.color.toRgba();
                    context.ellipse(startPosition.x, startPosition.y, endPosition.width, endPosition.height, 0, 0, 2 * Math.PI);
                    context.fill();
                    }
            }
        }

        private clearCanvasPreview() {
            if (this.previewCanvas) {
                const context = this.previewCanvas.getContext('2d');
                if (context) {
                    context.beginPath();
                    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
                }
            }
        }

    private resolveClientCoords(clientX: number, clientY: number): Coords {
        let coords;
        if (this.svgParentElement) {
            const svgBoundaries = this.svgParentElement.getBoundingClientRect();
            coords = new Coords(clientX - svgBoundaries.left, clientY - svgBoundaries.top);
        } else {
            coords = new Coords(clientX, clientY);
        }

        return coords;
    }

    public initialize(event: PointerEvent, svgElement: SVGSVGElement): void {
        this.svgParentElement = svgElement;
        this.svgParentElement.addEventListener('mousemove', this.onMouseMove);
        this.previewCanvas = document.getElementById(PREVIEW_CANVAS_ID) as HTMLCanvasElement
    }

    public create(): LayerInterface | undefined {
        let newLayer;
        if (this.svgParentElement) {
            this.svgParentElement.removeEventListener('mousemove', this.onMouseMove);
            if (this.cursorPositions.length) {
                let startPosition = this.cursorPositions[0];
                let endPosition = new Size(this.cursorPositions[this.cursorPositions.length-1].x,
                                            this.cursorPositions[this.cursorPositions.length-1].y);
                let tmpStartX = startPosition.x;
                let tmpStartY = startPosition.y
                let tmpEndX = endPosition.width;
                let tmpEndY = endPosition.height;
                if (startPosition.x > endPosition.width){
                    const tmpX = tmpStartX;
                    tmpStartX = tmpEndX;
                    tmpEndX = tmpX - tmpEndX;
                    tmpStartX += tmpEndX/2;
                    tmpEndX /= 2;
                }
                else {
                    tmpEndX -= tmpStartX;
                    tmpEndX /= 2;
                    tmpStartX += tmpEndX;
                }
                if (startPosition.y > endPosition.height){
                    const tmpY = tmpStartY;
                    tmpStartY = tmpEndY;
                    tmpEndY = tmpY - tmpEndY;
                    tmpStartY += tmpEndY/2;
                    tmpEndY /= 2;
                }
                else {
                    tmpEndY -= tmpStartY;
                    tmpEndY /= 2;
                    tmpStartY += tmpEndY;
                }
                startPosition = new Coords(tmpStartX, tmpStartY);
                endPosition = new Size(tmpEndX, tmpEndY);
                newLayer = new CircleLayer(startPosition, endPosition, this.color);
             }
            this.clear();
        }
        return newLayer;
    }

    public getName(): string {
        return 'Circle';
    }

    private clear(): void {
        this.cursorPositions = [];
        this.svgParentElement = undefined;
        this.clearCanvasPreview();
    }

    public getIconName(): string {
        return "circle";
    }

    public setColor(color: Color): void {
        this.color = color;
    }

    public getColor(): Color {
        return this.color;
    }
}