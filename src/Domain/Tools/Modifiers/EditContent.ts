import ModifierInterface from "./ModifierInterface";

export default class EditContent extends ModifierInterface{
    public getName(): string {
        return "Edit content";
    }

    public getLayerClassNames(): string[] {
        return ["text"];
    }

    public getIconName(): string {
        return "edit";
    }
}
