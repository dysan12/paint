import ToolInterface from "../ToolInterface";

export default abstract class ModifierInterface extends ToolInterface{
    public abstract getLayerClassNames(): string[];
}