import ModifierInterface from "./ModifierInterface";
import {ColorableInterface} from "../Settings/Color/ColorableInterface";
import Color from "../Settings/Color/Color";

export default class FillColor extends ModifierInterface implements ColorableInterface{
    private color = new Color(0,0,0);

    public getName(): string {
        return "Fill color";
    }

    public getLayerClassNames(): string[] {
        return ["fill_color"];
    }

    public getIconName(): string {
        return "fill-drip";
    }

    public getColor(): Color {
        return this.color;
    }

    public setColor(color: Color): void {
        this.color = color;
    }
}