import ModifierInterface from "./ModifierInterface";

export default class Drag extends ModifierInterface{
    public getName(): string {
        return "Drag";
    }

    public getLayerClassNames(): string[] {
        return ["draggable"];
    }

    public getIconName(): string {
        return "arrows-alt";
    }
}