import * as React from 'react';
import Toolbar from "./Toolbar/Toolbar";
import ToolsRegistryInterface from "../Domain/Tools/ToolsRegistryInterface";
import LayerInterface from "../Domain/Layers/Abstractions/LayerInterface";
import {LayersManager} from "./LayersManager/LayersManager";
import ToolInterface from "../Domain/Tools/ToolInterface";
import {Canvas} from "./Canvas/Canvas";
import CanvasSize from "./Canvas/CanvasSize";
import {BackgroundLayer} from "../Domain/Layers/BackgroundLayer";
import {ImmutableInterface} from "../Domain/Layers/Abstractions/ImmutableInterface";
import {VisibleInterface} from "../Domain/Layers/Abstractions/VisibleInterface";
import { SERVER_DOMAIN } from "../config";
import Deserializer from "../Domain/Layers/Deserializer";
import { Images } from "./Images/Images";

export interface AppProps {
    toolsRegistry: ToolsRegistryInterface;
    canvasSize: CanvasSize;
}

interface AppState {
    activeLayer: LayerInterface;
    layersStack: LayerInterface[];
    activeTool?: ToolInterface;
    availableImages: AvailableImages[];
    imageName: string;
}

export interface AvailableImages {
    _id: string; name: string; layers: object[];
}

export default class App extends React.Component<AppProps, AppState> {
    constructor(props: any) {
        super(props);

        const backgroundLayer = new BackgroundLayer(this.props.canvasSize);
        const layersStack = [backgroundLayer];

        this.state = {
            layersStack,
            activeLayer: backgroundLayer,
            activeTool: undefined,
            availableImages: [],
            imageName: "no_name"
        }
    }

    public changeActiveTool = (tool: ToolInterface): void => {
        this.setState((prevState) => {
            const activeTool = prevState.activeTool === tool ? undefined : tool;
            return {...prevState, activeTool};
        })
    };
    
    public addLayer = (layer: LayerInterface): void => {
        this.setState((prevState) => {
            prevState.layersStack.push(layer);
            return {...prevState}
        }, () => {this.changeActiveLayer(layer, this.state.activeTool)})
    };

    public removeLayer = (layerToDelete: LayerInterface): void => {
      this.setState((prevState) => {
            const layersStack = prevState.layersStack;
            for (let i = 0; i < layersStack.length; i++) {
                if (layerToDelete === layersStack[i]) {
                    layersStack.splice(i, 1);
                    break;
                }
            }

            return {...prevState}
      })
    };

    public changeActiveLayer = (layer: LayerInterface, activeTool: ToolInterface|undefined = undefined): void => {
        this.setState((prevState) => {
            return {...prevState, activeLayer: layer, activeTool};
        })
    };

    public toggleLayerVisibility = (layer: VisibleInterface): void => {
        this.setState((prevState) => {
            layer.setVisibility(!layer.isVisible());

            return {...prevState}
        })
    };

    public swapLayers = (layerIndexA: number, layerIndexB: number): void => {
        this.setState((prevState) => {
            const layersStack = prevState.layersStack;
            layersStack[layerIndexA] = layersStack.splice(layerIndexB, 1, layersStack[layerIndexA])[0];

            return {...prevState, layersStack}
        })
    };

    public toggleLayerImmutability = (layer: ImmutableInterface): void => {
        this.setState((prevState) => {
            layer.changeImmutabilityState(!layer.isImmutable());

            return {...prevState, activeTool: undefined}
        })
    };

    public refreshAvailableImages = async () => {
        await this.saveCurrentImage();
        await this.loadImages();
    };

    public componentDidMount() {
        this.loadImages();
    }

    public loadImages = async () => {
        try {
            const res = await fetch(`${SERVER_DOMAIN}/images`);
            const json = await res.json();

            this.setState((prevState) => {
                return {...prevState, availableImages: json}
            });

        } catch (e) {
            console.log(`Problem with receiving images occurred`);
        }
    };

    public loadImage = (image: AvailableImages) => {
        const layers = Deserializer.deserializeArray(image.layers);
        this.setState((prevState) => {
           return {...prevState, layersStack: layers, activeLayer: layers[0], activeTool: undefined, imageName: image.name}
        });
    };

    public deleteImage = async (image: AvailableImages) => {
        try {
            await fetch(
                `${SERVER_DOMAIN}/images/${image._id}`,
                {method: "DELETE"}
            );
            await this.loadImages();
            alert('An image has been deleted successfully');
        } catch (e) {
            alert('Something went wrong with deleting an image.')
        }
    };

    public saveCurrentImage = async () => {
        const name = prompt("Name of the image", this.state.imageName);
        if (name === null) {
            return;
        }

        const req = {
            name,
            layers: this.state.layersStack
        };

        try {
            const res = await fetch(
                `${SERVER_DOMAIN}/images`,
                {method: "POST",
                    body: JSON.stringify(req),
                    headers: {
                        "Content-Type": "application/json"
                    }}
            );
            const json = res.json();
            console.log(`Msg: ${json}`);
            this.setState((prevState) => {
               return {...prevState, imageName: name};
            });
            alert('Image has been saved successfully');
        } catch (e) {
            alert('Something went wrong with saving an image.')
        }
    };

    public render() {
        return (
            <main>
                <Toolbar
                    activeLayer={this.state.activeLayer}
                    activeTool={this.state.activeTool}
                    toolsRegistry={this.props.toolsRegistry}
                    changeActiveTool={this.changeActiveTool}
                    refresh={this.refreshAvailableImages}
                />
                <LayersManager
                    layersStack={this.state.layersStack}
                    activeLayer={this.state.activeLayer}
                    changeActiveLayer={this.changeActiveLayer}
                    removeLayer={this.removeLayer}
                    toggleLayerVisibility={this.toggleLayerVisibility}
                    toggleLayerImmutability={this.toggleLayerImmutability}
                    swapLayers={this.swapLayers}
                />
                <Canvas
                    layersStack={this.state.layersStack}
                    activeLayer={this.state.activeLayer}
                    activeTool={this.state.activeTool}
                    canvasSize={this.props.canvasSize}
                 addLayer={this.addLayer}/>
                <Images availableImages={this.state.availableImages} loadImage={this.loadImage}
                        deleteImage={this.deleteImage}/>
            </main>
        );
    }
}
