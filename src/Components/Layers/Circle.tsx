import {DraggableCore, DraggableData} from 'react-draggable';
import * as React from "react";
import Coords from "../../Domain/Layers/Common/Coords";
import Color from "../../Domain/Tools/Settings/Color/Color";

export interface CircleProps {
    position: Coords;
    width: number;
    height: number;
    isInDragMode: () => boolean;
    updatePosition: (position: Coords) => void;
    color: Color;
    getColor: () => Color;
    isInFillColorMode: () => boolean;
    attributes?: {};
}

interface CircleState {
    position: Coords
    color: Color
}

export class Circle extends React.Component<CircleProps, CircleState> {
    public constructor(props: CircleProps) {
        super(props);
        this.state = {
            position: this.props.position,
            color: this.props.color
        };
    }

    public tryToHandleDrag = (e: MouseEvent, data: DraggableData) => {
        if (this.props.isInDragMode()) {
            this.setState((prevState) => {
                const position = new Coords(
                    prevState.position.x + data.deltaX,
                    prevState.position.y + data.deltaY
                );

                this.props.updatePosition(position);
                return {...prevState, position}
            });
        }
    };

    public tryToHandleFill = () => {
        if (this.props.isInFillColorMode()) {
            const color = this.props.getColor();
            this.setState((prevState) => {
                return {...prevState, color}
            });
        }
    };

    public render() {
        return (
            <DraggableCore
                onDrag={this.tryToHandleDrag}
            >
                <ellipse
                    onClick={this.tryToHandleFill}
                    cx={this.state.position.x} cy={this.state.position.y}
                    rx={this.props.width} ry={this.props.height}
                    style={{fill:this.state.color.toRgba()}}
                    {...this.props.attributes}
                />
            </DraggableCore>
        );
    }
}