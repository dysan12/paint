import {DraggableCore, DraggableData} from 'react-draggable';
import * as React from "react";
import Coords from "../../Domain/Layers/Common/Coords";
import Color from "../../Domain/Tools/Settings/Color/Color";

export interface SquareProps {
    position: Coords;
    width: number;
    height: number;
    isInDragMode: () => boolean;
    updatePosition: (position: Coords) => void;
    color: Color;
    getColor: () => Color;
    isInFillColorMode: () => boolean;
    attributes?: {};
}

interface SquareState {
    position: Coords
    color: Color
}

export class Square extends React.Component<SquareProps, SquareState> {
    public constructor(props: SquareProps) {
        super(props);
        this.state = {
            position: this.props.position,
            color: this.props.color
        };
    }

    public tryToHandleDrag = (e: MouseEvent, data: DraggableData) => {
        if (this.props.isInDragMode()) {
            this.setState((prevState) => {
                const position = new Coords(
                    prevState.position.x + data.deltaX,
                    prevState.position.y + data.deltaY
                );

                this.props.updatePosition(position);
                return {...prevState, position}
            });
        }
    };

    public tryToHandleFill = () => {
        if (this.props.isInFillColorMode()) {
            const color = this.props.getColor();
            this.setState((prevState) => {
                return {...prevState, color}
            });
        }
    };

    public render() {
        return (
            <DraggableCore
                onDrag={this.tryToHandleDrag}
            >
                <rect
                    onClick={this.tryToHandleFill}
                    x={this.state.position.x} y={this.state.position.y}
                    width={this.props.width} height={this.props.height}
                    style={{fill:this.state.color.toRgba()}}
                    {...this.props.attributes}
                />
            </DraggableCore>
        );
    }
}