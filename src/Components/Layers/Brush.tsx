import {DraggableCore, DraggableData} from 'react-draggable';
import * as React from "react";
import Coords from "../../Domain/Layers/Common/Coords";
import Color from "../../Domain/Tools/Settings/Color/Color";
import Size from "../../Domain/Tools/Settings/Size/Size";

export interface BrushProps {
    position: Coords;
    pathCoords: Coords[];
    isInDragMode: () => boolean;
    updatePosition: (position: Coords, pathCoords: Coords[]) => void;
    color: Color;
    size: Size;
    attributes?: {};
}

interface BrushState {
    position: Coords;
    pathCoords: Coords[];
}

export class Brush extends React.Component<BrushProps, BrushState> {
    public constructor(props: BrushProps) {
        super(props);
        this.state = {
            position: this.props.position,
            pathCoords: this.props.pathCoords
        };
    }

    public tryToHandleDrag = (e: MouseEvent, data: DraggableData) => {
        if (this.props.isInDragMode()) {
            this.setState((prevState) => {
                const position = new Coords(
                    prevState.position.x + data.deltaX,
                    prevState.position.y + data.deltaY
                );
                const pathCoords = prevState.pathCoords.map((coords) => {
                    return new Coords(
                       coords.x + data.deltaX,
                       coords.y + data.deltaY
                   );
                })

                this.props.updatePosition(position, pathCoords);
                return {...prevState, position, pathCoords}
            });
        }
    };

    public render() {
        const paths = this.state.pathCoords.map((coords) => {
            return `L ${coords.x} ${coords.y}`;
        });
        const construction = `M${this.state.position.x} ${this.state.position.y} ${paths.join(' ')}`;
        return (
            <DraggableCore
                onDrag={this.tryToHandleDrag}
            >
                <path
                    d={construction}
                    fill={'none'}
                    strokeWidth={this.props.size.size}
                    stroke={this.props.color.toRgba()}
                    {...this.props.attributes}
                />
            </DraggableCore>
        );
    }
}