import {DraggableCore, DraggableData} from 'react-draggable';
import * as React from "react";
import Coords from "../../Domain/Layers/Common/Coords";
import Color from "../../Domain/Tools/Settings/Color/Color";
import Size from "../../Domain/Tools/Settings/Size/Size";

export interface LineProps {
    position: Coords;
    pathCoords: Coords;
    isInDragMode: () => boolean;
    updatePosition: (position: Coords, pathCoords: Coords) => void;
    color: Color;
    size: Size;
    attributes?: {};
}

interface LineState {
    position: Coords;
    pathCoords: Coords;
}

export class Line extends React.Component<LineProps, LineState> {
    public constructor(props: LineProps) {
        super(props);
        this.state = {
            position: this.props.position,
            pathCoords: this.props.pathCoords
        };
    }

    public tryToHandleDrag = (e: MouseEvent, data: DraggableData) => {
        if (this.props.isInDragMode()) {
            this.setState((prevState) => {
                const position = new Coords(
                    prevState.position.x + data.deltaX,
                    prevState.position.y + data.deltaY
                );
                const pathCoords = new Coords(
                    prevState.pathCoords.x + data.deltaX,
                    prevState.pathCoords.y + data.deltaY
                );

                this.props.updatePosition(position, pathCoords);
                return {...prevState, position, pathCoords}
            });
        }
    };

    public render(){
        return (
            <DraggableCore
                onDrag={this.tryToHandleDrag}
            >
                <line
                    x1={this.state.position.x}
                    y1={this.state.position.y}
                    x2={this.state.pathCoords.x}
                    y2={this.state.pathCoords.y}
                    strokeWidth={this.props.size.size}
                    stroke={this.props.color.toRgba()}
                    {...this.props.attributes}
                />
            </DraggableCore>
        );
    }
}