import {DraggableCore, DraggableData} from 'react-draggable';
import * as React from "react";
import Coords from "../../Domain/Layers/Common/Coords";
import Color from "../../Domain/Tools/Settings/Color/Color";
import Size from "../../Domain/Tools/Settings/Size/Size";

export interface TextProps {
    position: Coords;
    isInDragMode: () => boolean;
    updatePosition: (position: Coords) => void;
    color: Color;
    getColor: () => Color;
    size: Size;
    isInFillColorMode: () => boolean;
    attributes?: {};
    content: string[];
    isInEditContentMode: () => boolean;
    updateContent: (content: string[]) => void;
}

interface TextState {
    position: Coords
    color: Color
    size: Size
    content: string[]
}

export class Text extends React.Component<TextProps, TextState> {
    public constructor(props: TextProps) {
        super(props);
        this.state = {
            position: this.props.position,
            color: this.props.color,
            size: this.props.size,
            content: this.props.content
        };
    }

    public tryToHandleDrag = (e: MouseEvent, data: DraggableData) => {
        if (this.props.isInDragMode()) {
            this.setState((prevState) => {
                const position = new Coords(
                    prevState.position.x + data.deltaX,
                    prevState.position.y + data.deltaY
                );

                this.props.updatePosition(position);
                return {...prevState, position}
            });
        }
    };

    public tryToHandleFill = () => {
        if (this.props.isInFillColorMode()) {
            const color = this.props.getColor();
            this.setState((prevState) => {
                return {...prevState, color}
            });
        }
    };

    public tryToHandleChangeText = () => {
        if (this.props.isInEditContentMode()) {
            const input = prompt('New text', this.state.content.join('\n')) || '';
            const content = input !== '' ? input.split('\n') : ['Tekst'];
            this.setState((prevState) => {
               return {...prevState, content}
            }, () => this.props.updateContent(content));
        }
    };

    public render() {
        const size = this.props.size.size * 10;
        return (
            <DraggableCore
                onDrag={this.tryToHandleDrag}
            >
                <text
                    onClick={() => {
                        this.tryToHandleFill();
                        this.tryToHandleChangeText();
                    }}
                    x={this.state.position.x} y={this.state.position.y}
                    fontSize={size}
                    style={{fill:this.state.color.toRgba()}}
                    {...this.props.attributes}
                >
                    {this.state.content.map((text, index) =>
                        <tspan key={index} x={this.state.position.x} y={this.state.position.y + index * size}>{text}</tspan>
                    )}
                </text>
            </DraggableCore>
        );
    }
}
