import * as React from 'react';
import { AvailableImages } from "../App";
import './Images.scss';

export interface ImagesProps {
    availableImages: AvailableImages[];
    loadImage: (image: AvailableImages) => void;
    deleteImage: (image: AvailableImages) => void;
}

export class Images extends React.Component<ImagesProps, {}> {
    constructor(props: ImagesProps) {
        super(props);
    }

    public confirm(callback: any): void {
        if (confirm("Are you sure?")) {
            callback();
        }
    }

    public render() {
        return (
            <section id={"images"}>
                <ul>
                    {this.props.availableImages.map((avImage) => {
                        return <li key={avImage._id}>
                        <span onClick={
                            () => this.confirm(() => this.props.loadImage(avImage))
                        } className={"clickable"}>{avImage.name}</span>
                            <span onClick={
                                () => this.confirm(() => this.props.deleteImage(avImage))
                            } className={"clickable"}>[Remove]</span>
                        </li>;
                    })}
                </ul>
            </section>
        );
    }
}
