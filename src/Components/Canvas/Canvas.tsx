import * as React from 'react';
import ToolInterface from "../../Domain/Tools/ToolInterface";
import LayerInterface from "../../Domain/Layers/Abstractions/LayerInterface";
import CanvasSize from "./CanvasSize";
import ModifierInterface from "../../Domain/Tools/Modifiers/ModifierInterface";
import CreatorInterface from "../../Domain/Tools/Creators/CreatorInterface";
import './Canvas.scss';
import {instanceOfVisibleInterface} from "../../Domain/Layers/Abstractions/VisibleInterface";
import {SVG_ELEMENT_ID} from "../../config";

export interface CanvasProps {
    layersStack: LayerInterface[];
    canvasSize: CanvasSize;
    activeLayer?: LayerInterface;
    activeTool?: ToolInterface;
    addLayer: (layer: LayerInterface) => void;
}

export class Canvas extends React.Component<CanvasProps, {}> {
    private readonly svgRef: React.RefObject<SVGSVGElement>;

    constructor(props: CanvasProps) {
        super(props);
        this.svgRef = React.createRef();
    }

    public render() {
        const svgElements = this.props.layersStack.map((layer) => {
            if (instanceOfVisibleInterface(layer) && !layer.isVisible()) {
                return;
            }
            const attributes: {
                key: string;
                onMouseOver?: any;
                onMouseOut?: any;
                className?: string;
                onMouseDown?: any;
                onMouseUp?: any;
            } = {key: layer.getIdentifier()};

            if (
                this.props.activeTool instanceof ModifierInterface &&
                this.props.activeLayer &&
                this.props.activeLayer.getIdentifier() === layer.getIdentifier() &&
                layer.supportsModifier(this.props.activeTool)
            ) {
                attributes.className = this.props.activeTool.getLayerClassNames().join(" ");
                attributes.onMouseOver = () => layer.activateModifier(this.props.activeTool as ModifierInterface);
                attributes.onMouseOut = () => layer.deactivateModifier(this.props.activeTool as ModifierInterface);
            }
            return layer.asComponent(attributes)
        });

        const createAttributes : {onMouseDown?: any; onMouseUp?: any;} = {};
        if (this.props.activeTool instanceof CreatorInterface) {
            createAttributes.onMouseDown = (event: PointerEvent) => {
                if (this.props.activeTool instanceof CreatorInterface) {
                    const svg = this.svgRef.current;
                    if (svg) {
                        this.props.activeTool.initialize(event, svg);
                    }
                }
            };
            createAttributes.onMouseUp = () => {
                if (this.props.activeTool instanceof CreatorInterface) {
                    const newLayer = this.props.activeTool.create();
                    if (newLayer) {
                        this.props.addLayer(newLayer);
                    }
                }
            };
        }

        return (
            <section id={"canvasBox"}>
                <svg
                    id={SVG_ELEMENT_ID}
                    ref={this.svgRef}
                    width={this.props.canvasSize.getWidth()}
                    height={this.props.canvasSize.getHeight()}
                    {...createAttributes}
                >
                    {svgElements}
                </svg>
                <canvas id={'previewCanvas'} width={this.props.canvasSize.getWidth()} height={this.props.canvasSize.getHeight()} />
            </section>
        );
    }
}
