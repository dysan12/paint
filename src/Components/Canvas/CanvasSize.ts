export default class CanvasSize {
    public constructor(private readonly width: number, private readonly height: number) {
        this.width = width;
        this.height = height;
    }

    public getWidth(): number {
        return this.width;
    }

    public getHeight(): number {
        return this.height;
    }

    public static createFromObject(object: any): CanvasSize {
        if (object.width  !== undefined && object.height  !== undefined) {
            return new CanvasSize(parseInt(object.width, 10), parseInt(object.height, 10));
        } else {
            throw new Error(`Insufficient data for CanvasSize. Provided: ${JSON.stringify(object)}`);
        }
    }
}
