import * as React from 'react';
import './Toolbar.scss';
import ToolsRegistryInterface from "../../Domain/Tools/ToolsRegistryInterface";
import LayerInterface from "../../Domain/Layers/Abstractions/LayerInterface";
import ToolInterface from "../../Domain/Tools/ToolInterface";
import ModifierInterface from "../../Domain/Tools/Modifiers/ModifierInterface";
import CreatorInterface from "../../Domain/Tools/Creators/CreatorInterface";
import {instanceOfImmutableInterface} from "../../Domain/Layers/Abstractions/ImmutableInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {IconProp} from "@fortawesome/fontawesome-svg-core";
import {instanceOfColorableInterface} from "../../Domain/Tools/Settings/Color/ColorableInterface";
import ColorPicker from 'rc-color-picker';
import 'rc-color-picker/assets/index.css';
import ColorConverter from "./ColorConverter";
import {instanceOfSizeConfigurableInterface} from "../../Domain/Tools/Settings/Size/SizeConfigurableInterface";
import Size from "../../Domain/Tools/Settings/Size/Size";
import {SVG_ELEMENT_ID} from "../../config";
import {saveSvgAsPng} from 'save-svg-as-png';

export interface ToolbarProps {
    toolsRegistry: ToolsRegistryInterface;
    activeLayer: LayerInterface;
    activeTool?: ToolInterface;
    changeActiveTool: (tool: ToolInterface) => void;
    refresh: () => void;
}

export default class Toolbar extends React.Component<ToolbarProps> {
    constructor(props: any) {
        super(props);
    }

    private updateActiveToolColor = (compColor: {color: string, alpha: number}): void => {
      const color = ColorConverter.convertToColor(compColor);
      if (instanceOfColorableInterface(this.props.activeTool)) {
          this.props.activeTool.setColor(color);
      }
    };

    private updateActiveToolSize = (event: React.FormEvent<HTMLSelectElement>): void => {
        const size = parseInt(event.currentTarget.value, 10);
        if (instanceOfSizeConfigurableInterface(this.props.activeTool)) {
            this.props.activeTool.setSize(new Size(size));
        }
    };

    public render() {
        const modifiers = this.props.toolsRegistry.getModifiers().map(
            (modifier: ModifierInterface, index: number) => {
                const layerSupports = this.props.activeLayer.supportsModifier(modifier);
                if (
                    !layerSupports ||
                    (instanceOfImmutableInterface(this.props.activeLayer) && this.props.activeLayer.isImmutable())
                ) {
                    return (
                            <div key={index} className={"tool disabled"}>
                                <FontAwesomeIcon icon={modifier.getIconName() as IconProp}/>
                            </div>
                    );
                } else {
                    return (
                        <div
                            key={index}
                            onClick={() => this.props.changeActiveTool(modifier)}
                            className={`tool clickable ${this.props.activeTool === modifier ? 'btn-active' : ''}`}
                        >
                            <FontAwesomeIcon icon={modifier.getIconName() as IconProp}/>
                        </div>
                    );
                }
            }
        );

        const creators = this.props.toolsRegistry.getCreators().map(
            (creator: CreatorInterface, index: number) => {
                return (
                    <div
                        key={index}
                        onClick={() => this.props.changeActiveTool(creator)}
                        className={`clickable tool ${this.props.activeTool === creator ? 'btn-active' : ''}`}
                    >
                        <FontAwesomeIcon icon={creator.getIconName() as IconProp}/>
                    </div>
                );
            }
        );

        const configs = [];
        if (this.props.activeTool) {
            if (instanceOfColorableInterface(this.props.activeTool)) {
                const color = this.props.activeTool.getColor();
                configs.push(
                    <div key={'color'}>
                        <div>
                            <ColorPicker
                                animation="slide-up"
                                color={ColorConverter.convertToHex(color)}
                                alpha={color.alpha * 100}
                                onChange={this.updateActiveToolColor}
                            />
                        </div>
                    </div>
                );
            }
            if (instanceOfSizeConfigurableInterface(this.props.activeTool)) {
                const size = this.props.activeTool.getSize();
                const options = [];
                for (let i = 1; i <= 10; i++) {
                    options.push(
                        <option
                            key={i} value={i}
                        >{i}</option>
                    );
                }
                configs.push(
                    <div key={'size'}>
                        <span>Size:</span>
                        <select value={size.size} onChange={this.updateActiveToolSize}>
                            {options}
                        </select>
                    </div>
                );
            }
        }
        return (
            <section id={"toolbar"}>
                <div key={"left"} className={"left"}>
                    <div className={"creators"}>
                        {creators}
                    </div>
                    <div className={"modifiers"}>
                        {modifiers}
                    </div>
                    <div className={"config"}>
                        {configs}
                    </div>
                </div>
                <div key={"right"} className={"right"}>
                    <div className={"export"}>
                        {/*fixme retrieve element from Canvas component*/}
                        <div
                            className={"clickable"}
                            onClick={() => {
                              this.props.refresh();
                            }}
                        >
                            <span>Save to the server</span>
                        </div>
                        <div
                            className={"clickable"}
                            onClick={() => {saveSvgAsPng(document.getElementById(SVG_ELEMENT_ID), 'canvas.png')}}
                        >
                            <span>Save as PNG</span>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
