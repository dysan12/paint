import Color from "../../Domain/Tools/Settings/Color/Color";

export default class ColorConverter
{
    public static convertToHex(color: Color): string {
        return `#${color.red.toString(16)}${color.green.toString(16)}${color.blue.toString(16)}`;
    }

    public static convertToColor(color: {color: string, alpha: number}): Color {
        const rgba: {red: number, green: number, blue: number, alpha: number} = {red: 0, green:0, blue: 0, alpha: 1};
        const hexs = color.color.replace(/^#/,'').match(/.{1,2}/g);
        if (hexs && hexs.length === 3)
        {
            rgba.red = parseInt(hexs[0], 16);
            rgba.green = parseInt(hexs[1], 16);
            rgba.blue = parseInt(hexs[2], 16);
            rgba.alpha = color.alpha / 100;
        }

        return new Color(rgba.red, rgba.green, rgba.blue, rgba.alpha)
    }
}