import * as React from 'react';
import LayerInterface from "../../Domain/Layers/Abstractions/LayerInterface";
import './LayersManager.scss';
import {ImmutableInterface, instanceOfImmutableInterface} from "../../Domain/Layers/Abstractions/ImmutableInterface";
import {instanceOfVisibleInterface, VisibleInterface} from "../../Domain/Layers/Abstractions/VisibleInterface";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export interface LayersManagerProps {
    layersStack: LayerInterface[];
    activeLayer: LayerInterface;
    changeActiveLayer: (layer: LayerInterface) => void;
    removeLayer: (layerToDelete: LayerInterface) => void;
    toggleLayerVisibility: (layerToToggle: VisibleInterface) => void;
    toggleLayerImmutability: (layerToToggle: ImmutableInterface) => void;
    swapLayers: (layerIndexA: number, layerIndexB: number) => void;
}

export class LayersManager extends React.Component<LayersManagerProps> {
    constructor(props: LayersManagerProps) {
        super(props);
    }

    public render() {
        const layersElements = this.props.layersStack.map((layer: LayerInterface, index: number) => {
            const additions = [];
            if (instanceOfVisibleInterface(layer)) {
                additions.push(
                    <div
                        key={'hide-layer'}
                        onClick={() => this.props.toggleLayerVisibility(layer)}
                        className={`clickable ${!layer.isVisible() ? 'btn-active' : ''}`}
                    >
                        <FontAwesomeIcon icon="eye-slash"/>
                    </div>
                );
            }
            if (instanceOfImmutableInterface(layer)) {
                additions.push(
                  <div
                      key={'activate-layer'}
                    onClick={() => this.props.toggleLayerImmutability(layer)}
                    className={`clickable ${layer.isImmutable() ? 'btn-active' : ''}`}
                  >
                      <FontAwesomeIcon icon="link"/>
                  </div>
                );
            }
            return (
                <div
                    key={layer.getIdentifier()}
                    className={"layer"}
                >
                    <div className={"options"}>
                        <div key={'left'}>
                            <div key={'remove'} onClick={() => this.props.removeLayer(layer)} className={"clickable delete"}>
                                <FontAwesomeIcon icon="trash-alt"/>
                            </div>
                            {additions}
                        </div>
                        <div key={'right'}>
                            <div
                                key={'layer-down'}
                                className={index === this.props.layersStack.length - 1 ? 'btn-disabled' : 'clickable'}
                                onClick={index !== this.props.layersStack.length - 1 ? () => {this.props.swapLayers(index, index + 1)} : () => {}}
                            >
                                <FontAwesomeIcon icon="caret-down"/>
                            </div>
                            <div
                                key={'layer-up'}
                                className={index === 0 ? "btn-disabled" : 'clickable'}
                                onClick={index > 0 ? () => {this.props.swapLayers(index, index - 1)} : () => {}}
                            >
                                <FontAwesomeIcon icon="caret-up"/>
                            </div>
                        </div>
                    </div>
                    <div
                        onClick={() => this.props.changeActiveLayer(layer)}
                        className={"name clickable"}
                    >
                        <span>
                            {`#${index + 1} ${layer.getName()}`}
                            {this.props.activeLayer === layer ? '(Active)' : ''}
                        </span>
                    </div>
                </div>
            );
        });

        return (
            <section id={"layersManager"}>
                {layersElements}
            </section>
        );
    }
}