const express = require('express');
const mongoose = require('mongoose');

const router = express.Router();

const Image = mongoose.model('Image', { name: String, layers: [Object]});
/* GET users listing. */
router.get('/', function(req, res) {
  console.log('IMAGES GET');
  Image.find({}, (err, docs) => {
    if (docs) {
      res.json(docs);
    }
  });
}).post('/', async (req, res) => {
  const body = req.body;
  if (body.name && body.name !== '' && body.layers && body.layers !== '') {

    let asyncSave = async (image) => {
      console.log(image);
      try {
        await image.save();
        res.status(201);
        res.json({id: image._id})
      } catch (e) {
        console.log(e);
        res.status(400);
        res.json(e);
      }
    };

    try {
      const image = await Image.findOne({name: body.name}).exec();
      image.layers = body.layers;
      await asyncSave(image);
    } catch (e) {
      const image = new Image({name: body.name, layers: body.layers});
      await asyncSave(image);
    }

  } else {
    res.status(400);
    res.json({"message" : "Body request malformed"});
  }
});

router.route('/:id')
  .get(((req, res) => {
    const id = req.params.id;

    console.log('IMAGES GET ' + id);
    Image.findById(id, (err, image) => {
      console.log(image);
      if (image) {
        res.json(image);
      } else {
        res.status(404);
        res.json();
      }
    });
  }))
  .delete(async (req, res) => {
    const id = req.params.id;
    try {
      const del = await Image.deleteOne({'_id': id});
      res.status(204);
    } catch (e) {
      res.status(400);
    } 
    res.json();
  });




module.exports = router;
