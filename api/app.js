const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.connect('mongodb://localhost:27017/paint', { useNewUrlParser: true });
const imagesRouter = require('./routes/images');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/images', imagesRouter);

app.route('/', (req, res) => {
  console.log('start');
  res.json({});
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Listening`);
});
